import pathlib
from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5 import uic
from models.students_db import StudentDB

class StudentForm(QWidget):
    student_saved = pyqtSignal()
    def __init__(self) -> None:
        super().__init__()
        self.__studentHandler = StudentDB()
        mod_path = pathlib.Path(__file__).parent.parent
        uic.loadUi(mod_path/"views/student_form.ui", self)
        self._id = None
        self.saveButton.clicked.connect(lambda: self.save_student())
        self.cancelButton.clicked.connect(lambda: self.close())
        
    def save_student(self):
        if self._id:
            self.__studentHandler.update_student(
                self._id,
                self.firstNameTextField.text(),
                self.lastNameTextField.text(),
                self.emailTextField.text()
            )
        else:
            self.__studentHandler.create_student(
            self.firstNameTextField.text(),
            self.lastNameTextField.text(),
            self.emailTextField.text()
            )
        self.student_saved.emit()
        self.close()

    def load_student_data(self, id):
        self._id = id
        student_data = self.__studentHandler.get_students_by_id(id)
        if student_data:
            self.firstNameTextField.setText(student_data[1])
            self.lastNameTextField.setText(student_data[2])
            self.emailTextField.setText(student_data[3])

    def reset_form(self):
        self.firstNameTextField.setText("")
        self.lastNameTextField.setText("")
        self.emailTextField.setText("")
        self._id = None