import pathlib
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QCloseEvent
from PyQt5.QtWidgets import QMainWindow, QWidget, QTableWidgetItem, QMessageBox, QPushButton
from PyQt5 import uic, QtCore
from models.students_db import StudentDB
from controllers.student_form import StudentForm

class MainWindow(QMainWindow):
    
    def __init__(self) -> None:
        super().__init__()
        root_path = pathlib.Path(__file__).parent.parent
        uic.loadUi(root_path/"views/main_window.ui",self)
        self.__studentDBHandler = StudentDB()
        self._studentForm = StudentForm()
        self.newStudentAction.triggered.connect(lambda: self.create_student())
        self._studentForm.student_saved.connect(self.load_students)
        self.quitAction.triggered.connect(lambda:self.close())
        self.load_students()

    def load_students(self):
        students_list = self.__studentDBHandler.get_students()
        self.studentsTable.setRowCount(len(students_list))
        for i,student in enumerate(students_list):
            id,first_name,last_name,email = student
            self.studentsTable.setItem(i,0, QTableWidgetItem(str(id)))
            self.studentsTable.setItem(i,1, QTableWidgetItem(str(first_name)))
            self.studentsTable.setItem(i,2, QTableWidgetItem(str(last_name)))
            self.studentsTable.setItem(i,3, QTableWidgetItem(str(email)))
            self.studentsTable.item(i,0).setFlags(QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)          
            self.studentsTable.item(i,1).setFlags(QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
            self.studentsTable.item(i,2).setFlags(QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)    
            self.studentsTable.item(i,3).setFlags(QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable) 
            edit_button = QPushButton("Editar")
            edit_button.clicked.connect(self.edit_student)
            edit_button.setProperty("row", i)
            self.studentsTable.setCellWidget(i,4, edit_button)
            delete_button = QPushButton("Eliminar")
            delete_button.clicked.connect(self.delete_student)
            self.studentsTable.setCellWidget(i,5, delete_button)

    def edit_student(self):
        sender = self.sender()
        row = sender.property("row")
        id = self.studentsTable.item(row, 0).text()
        self._studentForm.load_student_data(id)
        self._studentForm.show()

    def delete_student(self, row):
        id = self.studentsTable.item(row, 0).text()
        msg = QMessageBox.question(self,'Confirmar eliminación',
            '¿Estás seguro de que deseas eliminar a este estudiante de la base de datos?',
            QMessageBox.Ok | QMessageBox.No)
        if msg == QMessageBox.Ok:  
            if self.__studentDBHandler.delete_student_by_id(id):
                self.studentsTable.removeRow(row)
                QMessageBox.information(self, "Listo", "Estudiante eliminado de la base de datos exitosamente.")
        else:
            pass

    def create_student(self):
        self._studentForm.reset_form()
        self._studentForm.show()

    def closeEvent(self, ev) -> None:
        self.__studentDBHandler.close()
        return super().closeEvent(ev)