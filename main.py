import sys
from PyQt5.QtWidgets import QApplication
from controllers.main_window import MainWindow

app = QApplication(sys.argv)
pyscopg_example = MainWindow()
pyscopg_example.show()
sys.exit(app.exec())